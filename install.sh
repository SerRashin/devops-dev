#!/usr/bin/env bash
sudo apt-get install -y dnsutils dnsmasq && \
sudo sed -i 's/#prepend domain-name-servers 127.0.0.1;/prepend domain-name-servers 127.0.0.1;/g' /etc/dhcp/dhclient.conf && \
sudo sed -i "s/nameserver 192.168.1.1/nameserver 127.0.0.1\nnameserver 192.168.1.1/g" /etc/resolv.conf && \
echo "address=/loc/127.0.0.1\naddress=/local/127.0.0.1\naddress=/dev/127.0.0.1\n\n\nlisten-address=127.0.0.1,192.168.0.1\n" | sudo tee --append /etc/dnsmasq.conf && \

echo 'alias sshDev="ssh -i /var/www/example.dev/docker/workspace/ssh/insecure_id_rsa root@localhost -p 2222"' | sudo tee --append ~/.zshrc && \
exec zsh