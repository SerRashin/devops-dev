
# Что входит в сборку ?
1. elasticsearch
2. memcached
3. mongo
4. MYSQL
5. nginx *
6. pgadmin
7. php-fpm 
8. phpmyadmin
9. postgres
10. rabbitMQ
11. redis
12. workspace *

# В планах
1. jenkins
2. logstash
3. kibana



# Нужны ли нам ?

1. Selenium - тестирование интерфейса.




# SSH авторизация
ssh -i path_to_insecure_id_rsa root@localhost -p 2222

### права на файлы
Для работоспособности права на файлы workspace/ssh/insecure_id_rsa и workspace/ssh/insecure_id_rsa.pub должны быть 600.


# workflow
- phpcs
- php